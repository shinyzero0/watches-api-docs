import { api, endpoint, headers, pathParams, request, response, body } from "@airtasker/spot";

@api({
	name: "time is ticking"
})
class Api { }

/**
 * tie user's vendor account with credentials (currently token only)
 *
 */
@endpoint({
	method: "POST",
	path: "/tie/:vendor"
})
class AddVendorToken {
	@request
	request(
		@body body: string,
		@headers headers: { Authorization: string },
		@pathParams pathParams: { vendor: string }
	) { }

	@response({ status: 200 }) successResponse(@body body: { accId: number }) { }
	/**
	 * in case of malformed body
	 *
	 */
	@response({ status: 400 }) badRequestResponse() { }
	@response({ status: 401 }) unauthorizedResponse() { }
	/**
	 * in case there is no such vendor
	 *
	 */
	@response({ status: 404 }) notFoundResponse() { }
}
/**
 * get the data from a tied vendor account by internal account id
 * as returned by /tie and /accounts
 */
@endpoint({
	method: "GET",
	path: "/data/:id"
})
class DataFromVendorByAccount {
	@request
	request(
		@headers headers: { Authorization: string },
		@pathParams pathParams: { id: number }
	) { }
	@response({ status: 200 }) successResponse(@body body: { data: {} }) { }
	@response({ status: 401 }) unauthorizedResponse() { }
	@response({ status: 403 }) forbiddenResponse() { }
	/**
	 * in case there is no such account
	 *
	 */
	@response({ status: 404 }) notFoundResponse() { }
}
/**
 * get a list of vendor accounts tied to authorized user
 *
 */
@endpoint({
	method: "GET",
	path: "/accounts/"
})
class ListAccounts {
	@request
	request(
		@headers headers: { Authorization: string },
	) { }
	@response({ status: 200 }) successResponse(@body body: Array<{id: number, vendor: string}>) { }
	@response({ status: 401 }) unauthorizedResponse() { }
}

