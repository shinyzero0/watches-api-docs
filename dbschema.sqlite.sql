CREATE TABLE vendors (
	id INTEGER PRIMARY KEY,
	api_endpoint VARCHAR(1024),
	developer_credentials VARCHAR(1024),
	name VARCHAR(256)
);
CREATE TABLE users (
	id INTEGER PRIMARY KEY,
	login VARCHAR(256),
	password VARCHAR(60) -- bcrypt
);
CREATE TABLE tied_accounts (
	id INTEGER PRIMARY KEY,
	owner_id INTEGER REFERENCES users(id),
	vendor_id INTEGER REFERENCES vendors(id),
	credentials VARCHAR(1024)
);
